import datetime

def create_chat(txn, owner):
    txn.execute("INSERT INTO chats (owner) VALUES (?)", (owner,))

    chat_id = None
    if txn.lastrowid is not None:
        chat_id = txn.lastrowid

    return chat_id

def delete_chat(txn, chat_id):
    txn.execute("DELETE FROM chats WHERE id=?", (chat_id,))

    return True

def add_user_to_chat(txn, chat_id, user_id):
    txn.execute("INSERT INTO chat_participants (chat_id, participant) VALUES (?, ?)", (chat_id, user_id) )

    if txn.lastrowid is not None:
        return True

    return False

def delete_chat_participants(txn, chat_id):
    txn.execute("DELETE FROM chat_participants WHERE chat_id=?", (chat_id,))

    return True

def delete_user_from_chat(txn, chat_id, user_id):
    txn.execute("DELETE FROM chat_participants WHERE chat_id=? and participant=?", (chat_id, user_id))

    return True

def get_participants_in_chat(txn, chat_id):
    txn.execute("SELECT participant FROM chat_participants WHERE chat_id=?", (chat_id,) )
    results = txn.fetchall()
    if results:
        return [result[0] for result in results]

    return None

def create_pending_message(txn, chat_id, recipient_id, sender, message_body, time):
    txn.execute("INSERT INTO pending_chat_messages (chat_id, recipient_id, sender, message, time) VALUES (?,?,?,?,?)",
                (chat_id, recipient_id, sender, message_body, time))

    message_id = None
    if txn.lastrowid is not None:
        message_id = txn.lastrowid

    return message_id

def create_pending_chat_notification(txn, chat_id, recipient_id, chat_event, username, time):
    txn.execute("INSERT INTO pending_chat_notifications (chat_id, recipient_id, chat_event, username, time) VALUES (?,?,?,?,?)",
                (chat_id, recipient_id, chat_event, username, time))

    message_id = None
    if txn.lastrowid is not None:
        message_id = txn.lastrowid

    return message_id

def get_chat_owner(txn, chat_id):
    txn.execute("SELECT owner FROM chats WHERE id=?", (chat_id,))

    result = txn.fetchone()

    if result:
        return result[0]

    return None

def find_chat_between_users(txn, sender_id, recipient_id):
    sender_chats = set()
    recipient_chats = set()

    txn.execute("SELECT chat_id from chat_participants WHERE participant=?", (sender_id,))
    results = txn.fetchall()
    if results:
        for result in results:
            sender_chats.add(result[0])

    txn.execute("SELECT chat_id from chat_participants WHERE participant=?", (recipient_id,))
    results = txn.fetchall()
    if results:
        for result in results:
            recipient_chats.add(result[0])

    intersection = sender_chats.intersection(recipient_chats)

    if len(intersection) == 0:
        return None

    return intersection.pop()
