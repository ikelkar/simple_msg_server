__author__ = 'ikelkar'

class response:
    def __init__(self, command, command_data):
        self.command = command
        self.command_data = command_data

class send:
    def __init__(self, chat_id, sender, recipients, data):
        #chat_id 0 is a system message
        self.chat_id = chat_id
        self.sender = sender
        self.recipients = recipients
        self.data = data

class outbound:
    def __init__(self, response=None, send_obj_list=None):
        self.response_obj = response
        self.send_obj_list = []
        if send_obj_list is not None:
            self.send_obj_list = [send_obj for send_obj in send_obj_list]

    def add_send_obj(self, send_obj):
        if hasattr(self, "send_obj_list"):
            self.send_obj_list.append(send_obj)
        else:
            self.send_obj_list = [send_obj]