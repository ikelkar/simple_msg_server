import json
import pdb

class MessageTypes:
    SERVER_MESSAGE = "SERVER_MESSAGE"
    CLIENT_COMMAND = "CLIENT_COMMAND"
    RESPONSE = "RESPONSE"
    CHAT_MESSAGE = "CHAT_MESSAGE"
    STATUS_NOTIFICATION = "STATUS_NOTIFICATION"
    CHAT_NOTIFICATION = "CHAT_NOTIFICATION"

class Message(object):
    def __init__(self, message_type):
        self.__message_type = message_type

    def get_message_type(self):
        return self.__message_type

    def to_json(self):
        data = dict()
        for attribute_key in vars(self):
            #If private, skip
            if "__" in attribute_key:
                continue

            attribute = getattr(self, attribute_key)

            #If a dict, list (and I think this would work for set as well) convert to json
            #if hasattr(attribute, "__iter__"):
                #data[attribute_key] = json.dumps(attribute)
            #else:
            data[attribute_key] = attribute

        return json.dumps({"message_type": self.get_message_type(), "data": data})

class ServerMessage(Message):

    class ServerMessageTypes:
        UNKNOWN_ERROR = "UNKNOWN_ERROR"
        UNKNOWN_MESSAGE = "UNKNOWN_MESSAGE"

    def __init__(self, server_message_type, detail_message):
        #Command that this is a response to
        super(ServerMessage, self).__init__(MessageTypes.SERVER_MESSAGE)

        self.server_message_type = server_message_type
        self.detail_message = detail_message

class Response(Message):

    class CommandStatus:
        SUCCESS = "SUCCESS"
        FAIL = "FAIL"

    def __init__(self, command, command_status, detailed_message=""):
        #Command that this is a response to
        super(Response, self).__init__(MessageTypes.RESPONSE)

        self.command = command
        self.command_status = command_status
        self.detailed_message = detailed_message
        self.data = dict()

    def add_kvp_to_data(self, key, value):
        self.data[key] = value

class ChatMessage(Message):

    def __init__(self, chat_id, sender, message, time):
        #Command that this is a response to
        super(ChatMessage, self).__init__(MessageTypes.CHAT_MESSAGE)

        self.chat_id = chat_id
        #Sender must be a username
        self.sender = sender
        self.message = message
        self.time = time

class StatusNotification(Message):

    class Notification:
        ONLINE = "ONLINE"
        OFFLINE = "OFFLINE"

    def __init__(self, user, status):
        #Command that this is a response to
        super(StatusNotification, self).__init__(MessageTypes.STATUS_NOTIFICATION)

        self.user = user
        self.status = status

class ChatNotification(Message):

    class Notification:
        CREATED_CHAT = "CREATED_CHAT"
        DELETED_CHAT = "DELETED_CHAT"
        JOINED_CHAT = "JOINED_CHAT"
        LEFT_CHAT = "LEFT_CHAT"

    def __init__(self, chat_id, event, user, time):
        #Command that this is a response to
        super(ChatNotification, self).__init__(MessageTypes.CHAT_NOTIFICATION)

        self.chat_id = chat_id
        self.event = event
        self.user = user
        self.time = str(time)

class Outbound:
    def __init__(self, recipient, message):
        #Recipient must be an IP
        self.recipient = recipient
        self.message = message

def single_message_multiple_recipients(message, recipients):
    outbound_list = list()
    for recipient in recipients:
        outbound_list.append(Outbound(recipient, message))
    return outbound_list
