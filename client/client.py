import time,readline,thread,sys
import telnetlib
import struct,fcntl,termios

server_address = sys.argv[1]
port = sys.argv[2]

tn = telnetlib.Telnet(server_address, port)
username = raw_input('Enter username: ')
tn.write('{"command": "connect", "command_data":{"username": "' + username + '"}}\n')

def print_server_response_thread():
    try:
        while True:
            time.sleep(1)
            output = tn.read_until("\n",1)
            if output is not None and output != "":
                print "\nReceived: " + output
                line_buf = readline.get_line_buffer()

                sys.stdout.write('> ' + line_buf)
                sys.stdout.flush()
            output = None
    except:
        exit(1)

thread.start_new_thread(print_server_response_thread, ())
while True:
    print("Available commands:")
    print("1: Create Chat")
    print("2: Send Message to Chat")
    print("3: Add User to chat")
    print("4: Remove User from chat")
    print("5: Leave chat")
    print("6: Delete Chat")
    print("7: Dumb Send Message")
#    command = raw_input("Enter Command:")
    s = raw_input('> ')

    if s == "1":
        #Create Chat
        users = []
        q = ""
        while True:
            q = raw_input('Enter users to chat with. q to end: ')
            if q == "q":
                break
            users.append(q)

        json_msg = '{"command": "create_chat", "command_data":{"chat_participants": ["' + '", "'.join(users) + '"] }}\n'
        print "Sent: " + json_msg
        tn.write(json_msg)

    elif s == "2":
        c = raw_input('Enter chat id to send message to: ')
        msg = raw_input('Enter msg: ')
        json_msg = '{"command": "send_message", "command_data":{"chat_id":' + c + ', "chat_msg": "' + msg + '"}}\n'
        print "Sent: " + json_msg
        tn.write(json_msg)

    elif s == "3":
        c = raw_input('Enter chat id to add user to: ')
        uname = raw_input('Username: ')
        json_msg = '{"command": "add_user_to_chat", "command_data":{"chat_id":' + c + ', "username": "' + uname +'"}}\n'
        print "Sent: " + json_msg
        tn.write(json_msg)

    elif s == "4":
        c = raw_input('Enter chat id to remove user from: ')
        uname = raw_input('Username: ')
        json_msg = '{"command": "remove_user_from_chat", "command_data":{"chat_id":' + c + ', "username": "' + uname +'"}}\n'
        print "Sent: " + json_msg
        tn.write(json_msg)

    elif s == "5":
        c = raw_input('Leave chat: ')
        json_msg = '{"command": "leave_chat", "command_data":{"chat_id":' + c +'}}\n'
        print "Sent: " + json_msg
        tn.write(json_msg)

    elif s == "6":
        c = raw_input('Chat id to delete: ')
        json_msg = '{"command": "delete_chat", "command_data":{"chat_id": ' + c + '}}\n'
        print "Sent: " + json_msg
        tn.write(json_msg)

    elif s == "7":
        rec = raw_input('Enter recipient username: ')
        msg = raw_input('Enter msg: ')
        json_msg = '{"command": "dumb_send_message", "command_data":{"recipient":"' + rec + '", "chat_msg": "' + msg + '"}}\n'
        print "Sent: " + json_msg
        tn.write(json_msg)
