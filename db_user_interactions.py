import datetime
import pdb

def create_user(txn, username, ip):
    txn.execute("INSERT INTO users VALUES (NULL, ?, ?, ?, ?)", (username, 1, ip, datetime.datetime.now()))
    if txn.lastrowid is not None:
        return True

    return False

def update_user_info(txn, user_id, ip, online_status):
    '''Updates ip, online status, time of last modification for user'''
    #pdb.set_trace()
    txn.execute("UPDATE users SET ip=?, online=?, status_last_updated=? WHERE id=?", (ip,
                                                                                      online_status,
                                                                                      datetime.datetime.now(),
                                                                                      user_id))

    return True

def user_online(txn, user_id):
    txn.execute("SELECT online FROM users WHERE id=?", (user_id,))
    result = txn.fetchone()

    if result:
        if result[0] == 1:
            return True

    return False

def get_user_id_from_ip(txn, ip):
    txn.execute("SELECT id FROM users WHERE ip=?", (ip,))
    result = txn.fetchone()

    if result:
        return result[0]

    return False

def get_user_ip_from_id(txn, user_id):
    txn.execute("SELECT ip FROM users WHERE id=?", (user_id,))
    result = txn.fetchone()

    if result:
        return result[0]

    return False

def get_username_from_id(txn, user_id):
    txn.execute("SELECT name FROM users WHERE id=?", (user_id,))
    result = txn.fetchone()

    if result:
        return result[0]

    return False

def get_user_id_from_username(txn, username):
    '''Returns user_id if username exists or None'''
    txn.execute("SELECT id FROM users WHERE name=?", (username,) )
    results = txn.fetchall()

    if results:

        #Username should be unique
        if len(results) > 1:
            #Throw error
            pass

        return results[0][0]

    return None

def get_pending_messages_for_user(txn, user_id):
    txn.execute("SELECT pending_chat_messages.chat_id, sender, message, time "
                "FROM pending_chat_messages "
                "WHERE recipient_id=?", (user_id,))

    results = txn.fetchall()
    pending_messages = list()
    if results:
        for result in results:
            pending_messages.append({"chat_id": result[0], "sender": result[1], "message": result[2], "time": result[3]})

        #Delete these messages once they have been extracted
        txn.execute("DELETE FROM pending_chat_messages WHERE recipient_id=?", (user_id,))
        return pending_messages

    return None

def get_pending_chat_notifications_for_user(txn, user_id):
    txn.execute("SELECT recipient_id, chat_id, chat_event, username, time "
                "FROM pending_chat_notifications "
                "WHERE recipient_id=?", (user_id,))

    results = txn.fetchall()
    pending_messages = list()
    if results:
        for result in results:
            pending_messages.append({"recipient_id": result[0], "chat_id": result[1], "chat_event": result[2], "username": result[3], "time": result[4]})

        #Delete these messages once they have been extracted
        txn.execute("DELETE FROM pending_chat_notifications WHERE recipient_id=?", (user_id,))
        return pending_messages

    return None

def in_chats_with_users(txn, user_id):
    txn.execute("SELECT DISTINCT contact.participant " +
                "FROM chat_participants user " +
                "JOIN chat_participants contact " +
                "ON user.chat_id=contact.chat_id WHERE user.participant=?", (user_id,))
    results = txn.fetchall()

    if results:
        #This user does have some conversations open
        users = list()
        for result in results:
            if result[0] == user_id:
                continue
            users.append(result[0])
        return users

    return None
