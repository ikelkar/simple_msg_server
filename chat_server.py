import sys
from twisted.python import log
from twisted.internet import reactor
from twisted.internet.protocol import ServerFactory
from twisted.protocols.basic import LineReceiver
from twisted.enterprise import adbapi
import json

from outgoing import Response, ServerMessage, Outbound
from chat_exception import ChatException, WrapperException
import db_interactions

#debug
import pdb

event_handlers = dict()
def register_for_command(path):
    def add_event_handler(fcn):
        event_handlers[path] = fcn
        return fcn
    return add_event_handler

class chat_protocol(LineReceiver):
    delimiter = "\n"

    #<Connection handlers>
    def connectionMade(self):
        '''Connection attempt. If we haven't reached the max number of clients yet, accept it. '''
        #pdb.set_trace()
        client_ip = self.transport.getPeer().host + ":" + str(self.transport.getPeer().port)
        log.msg("Client %s attempting to connect" % client_ip)

        if len(self.factory.clients) >= self.factory.clients_max:
            log.msg("Too many connections. Refusing %s" % client_ip)
            self.transport.loseConnection()
        else:
            log.msg("Accepting connection from %s" % client_ip)
            self.factory.clients.append(self)
            self.ip = client_ip

    def connectionLost(self, reason):
        '''Lost connection with client.'''
        log.msg('Lost client connection with %s. Reason: %s' % (self.ip, reason))
        self.factory.clients.remove(self)

        return dbpool.runInteraction(db_interactions.disconnect, self.ip).addCallback(self.handle_outgoing_communication)


    def lineReceived(self, line):
        '''Got a command from a client. All messages MUST be terminated with a '\n' '''
        log.msg('Cmd received from %s : %s' % (self, line))
        try:
            data = json.loads(line)
            log.msg(data)
            self.handle_event(data)
        except ValueError:
            self.failure_response(WrapperException(ChatException("unknown", "Malformed JSON message.")))

    #</Connection handlers>

    #<Event handlers>
    @register_for_command("connect")
    def connect(self, data):
        return dbpool.runInteraction(db_interactions.connect, self.ip, data["username"])

    @register_for_command("create_chat")
    def create_chat(self, data):
        return dbpool.runInteraction(db_interactions.create_chat, self.ip, data["chat_participants"])

    @register_for_command("send_message")
    def send_message(self, data):
        return dbpool.runInteraction(db_interactions.send_message, self.ip, data["chat_id"], data["chat_msg"])

    @register_for_command("dumb_send_message")
    def send_message_single_user_no_id(self, data):
        return dbpool.runInteraction(db_interactions.send_message_single_user_no_id, self.ip, data["recipient"], data["chat_msg"])

    @register_for_command("delete_chat")
    def delete_chat(self, data):
        return dbpool.runInteraction(db_interactions.delete_chat, self.ip, data["chat_id"])

    @register_for_command("add_user_to_chat")
    def add_user_to_chat(self, data):
        return dbpool.runInteraction(db_interactions.add_user_to_chat, self.ip, data["chat_id"], data["username"])

    @register_for_command("remove_user_from_chat")
    def remove_user_from_chat(self, data):
        return dbpool.runInteraction(db_interactions.remove_user_from_chat, self.ip, data["chat_id"], data["username"])

    @register_for_command("leave_chat")
    def leave_chat(self, data):
        return dbpool.runInteraction(db_interactions.leave_chat, self.ip, data["chat_id"])

    def handle_event(self, data):
        '''Call appropriate event handler, and send responses'''
        #pdb.set_trace()
        if data["command"] in event_handlers:
            method_to_call = event_handlers[str(data["command"])]
            d = method_to_call(self, data["command_data"])
            d.addCallback(self.handle_outgoing_communication)
            d.addErrback(self.failure_response)
        else:
            self.server_error(ServerMessage.ServerMessageTypes.UNKNOWN_MESSAGE, "Unknown message %s" % (data["command"],))

    #</Event Handlers>

    #<Outgoing Message Handlers>
    def find_client_from_ip(self, recipient_ip):
        for c in self.factory.clients:
            if c.ip == recipient_ip:
                return c

        return None

    def handle_outgoing_communication(self, outbound_list):
        #pdb.set_trace()
        for outbound in outbound_list:
            client = self.find_client_from_ip(outbound.recipient)
            if client is not None:
                client.transport.write(outbound.message.to_json() + "\n")

    def server_error(self, error_type, detailed_message):
        ''' Something went wrong.
            This function: -Creates a server error message and sends it to the user
        '''
        o = Outbound(self.ip, ServerMessage(error_type, detailed_message))

        self.handle_outgoing_communication([o])

    def failure_response(self, e):
        ''' This function is called if any of the event handlers fail to send back.
            This function:  -Sends back a failure response
        '''
        command = e.value.command
        detail_message = e.value.description
        o = Outbound(self.ip, Response(command=command,
                                       command_status= Response.CommandStatus.FAIL,
                                       detailed_message=detail_message))

        self.handle_outgoing_communication([o])

    #</Outgoing Message Handlers>

class chatserver_factory(ServerFactory):
    protocol = chat_protocol

    def __init__(self, clients_max=10):
        self.clients_max = clients_max
        self.clients = []

log.startLogging(sys.stdout)
reactor.listenTCP(int(sys.argv[1]), chatserver_factory(100))
dbpool = adbapi.ConnectionPool("sqlite3", sys.argv[2])
reactor.run()