__author__ = 'ik'

class ChatException(Exception):
    def __init__(self, command, description):
        self.command = command
        self.description = description

    def __str__(self):
        return "%s - %s" % (self.command, self.description)

class WrapperException:
    def __init__(self, ce):
        self.value = ce