import db_user_interactions as usr_ixn
import db_chat_interactions as chat_ixn
from outgoing import Outbound, Response, StatusNotification, ChatNotification, ChatMessage, single_message_multiple_recipients
import datetime
from chat_exception import ChatException

def connect(txn, ip, username):
    ''' This Function:
        -Creates a user or sets an existing user as online
        -Sends users who have existing chats with this user online notification messages
        -Sends existing user and pending messages
    '''
    response_command = "connect"
    outgoing_messages = list()

    try:
        user_id = usr_ixn.get_user_id_from_username(txn, username=username)

        if user_id is not None:

            usr_ixn.update_user_info(txn, user_id=user_id, ip=ip, online_status=1)

            #Create response object
            outgoing_messages.append(Outbound(ip, Response(response_command, Response.CommandStatus.SUCCESS)))

            users_with_open_chats = usr_ixn.in_chats_with_users(txn, user_id=user_id)

            if users_with_open_chats is not None:
                #Create notification objects for these users who are online
                online_users_with_open_chats = filter(lambda x: usr_ixn.user_online(txn, x), users_with_open_chats)
                users_with_open_chats_ips = map(lambda x: usr_ixn.get_user_ip_from_id(txn, x), online_users_with_open_chats)
                outgoing_messages += single_message_multiple_recipients(StatusNotification(username,
                                                                                           StatusNotification.Notification.ONLINE),
                                                                        users_with_open_chats_ips)

                for user_with_open_chat in online_users_with_open_chats:
                    outgoing_messages.append(Outbound(ip,
                                                      StatusNotification(usr_ixn.get_username_from_id(txn, user_id=user_with_open_chat),
                                                                         StatusNotification.Notification.ONLINE)))


            pending_messages_and_notifications = list()

            #Get pending chat notifications
            pending_chat_notifications = usr_ixn.get_pending_chat_notifications_for_user(txn, user_id=user_id)

            if pending_chat_notifications is not None:
                for pending_notification in pending_chat_notifications:
                    pending_messages_and_notifications.append(Outbound(ip,
                                                      ChatNotification(pending_notification["chat_id"],
                                                                       pending_notification["chat_event"],
                                                                       pending_notification["username"],
                                                                       pending_notification["time"])))


            #Send pending messages
            pending_messages = usr_ixn.get_pending_messages_for_user(txn, user_id=user_id)

            if pending_messages is not None:
                for pending_message in pending_messages:
                    pending_messages_and_notifications.append(Outbound(ip,
                                                      ChatMessage(pending_message["chat_id"],
                                                                  pending_message["sender"],
                                                                  pending_message["message"],
                                                                  pending_message["time"])))

            outgoing_messages += sorted(pending_messages_and_notifications, key=lambda x: x.message.time)

        else:

            if usr_ixn.create_user(txn, username=username, ip=ip):
                #Create response
                outgoing_messages.append(Outbound(ip, Response(response_command, Response.CommandStatus.SUCCESS)))
            else:
                raise ChatException(response_command, "Creating new user failed")

    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages

def disconnect(txn, ip):
    ''' This function:
        -Sets the user with the given ip as offline
        -Sends users who have existing chats with this user offline notification messages
    '''
    response_command = "disconnect"
    outgoing_messages = list()

    try:

        user_id = usr_ixn.get_user_id_from_ip(txn, ip=ip)

        #If this condition is not met it means the user never submitted a "connect" message
        if user_id is not None:

            usr_ixn.update_user_info(txn, user_id=user_id, ip="", online_status=0)

            users_with_open_chats = usr_ixn.in_chats_with_users(txn, user_id=user_id)

            if users_with_open_chats is not None:
                #Create notification objects for these users who are online
                online_users_with_open_chats = filter(lambda x: usr_ixn.user_online(txn, x), users_with_open_chats)
                users_with_open_chats_ips = map(lambda x: usr_ixn.get_user_ip_from_id(txn, x), online_users_with_open_chats)

                username = usr_ixn.get_username_from_id(txn, user_id)
                outgoing_messages = single_message_multiple_recipients(StatusNotification(username,
                                                                                          StatusNotification.Notification.OFFLINE),
                                                                       users_with_open_chats_ips)

    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages

def create_chat(txn, ip, participants):
    ''' This function:  -Creates a chat and returns the chat_id in a response
                        -Adds listed user to newly created chat
    '''
    outgoing_messages = list()
    response_command = "create_chat"

    try:
        creator = usr_ixn.get_user_id_from_ip(txn, ip=ip)

        if creator is not None:
            creator_username = usr_ixn.get_username_from_id(txn, user_id=creator)
            chat_id = chat_ixn.create_chat(txn, creator)
            if chat_id is None:
                raise ChatException(response_command, "Creating chat failed")

            #Add all users to chat_participants table
            chat_ixn.add_user_to_chat(txn, chat_id=chat_id, user_id=creator)

            #Map of participant id to ip
            participants_to_id = dict()
            participants_to_ip = dict()
            online_participants = dict()

            for participant in participants:
                participant_id = usr_ixn.get_user_id_from_username(txn, participant)

                if participant_id is not None:
                    participants_to_id[participant] = participant_id
                    chat_ixn.add_user_to_chat(txn, chat_id=chat_id, user_id=participant_id)
                    participants_to_ip[participant] = usr_ixn.get_user_ip_from_id(txn, participant_id)

                    if usr_ixn.user_online(txn, participant_id):
                        online_participants[participant]= participants_to_ip[participant]
                else:
                    raise ChatException(response_command, "Could not find user %s to add to chat" % (participant,))

            #Handle the JOINED_CHAT notifications
            #Create JOINED_CHAT notifications for people in the chat
            #U = Complete list of users in chat
            #U = C (creator) + O (others)
            #for p in O, all users O - p receive a notification that p Joined
            #C does not get notifications about which users joined the chat since they specify which users are in the chat.
            #If one or more of the users could not be added to the chat this txn should fail
            for participant in participants:

                if participant in online_participants:
                    outgoing_messages.insert(0,Outbound(participants_to_ip[participant],
                                                        ChatNotification(chat_id,
                                                                         ChatNotification.Notification.CREATED_CHAT,
                                                                         creator_username,
                                                                         datetime.datetime.now())))
                else:
                    chat_ixn.create_pending_chat_notification(txn,
                                                              chat_id=chat_id,
                                                              recipient_id=participants_to_id[participant],
                                                              chat_event=ChatNotification.Notification.CREATED_CHAT,
                                                              username=creator_username,
                                                              time=datetime.datetime.now())

            for participant in participants:

                other_participants = list(participants)
                other_participants.remove(participant)

                for other_participant in other_participants:
                    if other_participant in online_participants:
                        outgoing_messages.append(Outbound(participants_to_ip[other_participant],
                                                          ChatNotification(chat_id,
                                                                           ChatNotification.Notification.JOINED_CHAT,
                                                                           participant,
                                                                           datetime.datetime.now())))
                    else:
                        chat_ixn.create_pending_chat_notification(txn,
                                                                  chat_id=chat_id,
                                                                  recipient_id=participants_to_id[other_participant],
                                                                  chat_event=ChatNotification.Notification.JOINED_CHAT,
                                                                  username=participant,
                                                                  time=datetime.datetime.now())

        else:
            raise ChatException(response_command, "User not connected")

        #Create response object
        o = Outbound(ip, Response(response_command, Response.CommandStatus.SUCCESS))
        o.message.add_kvp_to_data("chat_id", chat_id)
        outgoing_messages.insert(0, o)

    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages

#TODO
def delete_chat(txn, ip, chat_id):
    #Create chat notifications for everyone (except the creator) in the chat
    response_command = "delete_chat"
    outgoing_messages = list()

    try:
        deleter = usr_ixn.get_user_id_from_ip(txn, ip=ip)

        if deleter is not None:
            deleter_username = usr_ixn.get_username_from_id(txn, deleter)
            chat_owner = chat_ixn.get_chat_owner(txn, chat_id)

            if chat_owner is None:
                raise ChatException(response_command, "Unable to get chat owner for chat_id %s" % (chat_id,))

            if deleter == chat_owner:
                #Delete chat has to: -Delete the chat_id from the chats table
                #                    -Delete the participants from the chat_participants table
                #Get all the chat participants for the chat (except the creator, send them a chat notification that it has
                #been deleted.
                #Remove all the participants
                #Remove the chat_id
                other_participants_ids = chat_ixn.get_participants_in_chat(txn, chat_id=chat_id)
                other_participants_ids.remove(deleter)
                chat_ixn.delete_chat_participants(txn, chat_id)
                chat_ixn.delete_chat(txn, chat_id)
            else:
                raise ChatException(response_command, "Only creator can delete chat")

            outgoing_messages.append(Outbound(ip, Response(response_command, Response.CommandStatus.SUCCESS)))

            #Create chat notifications for all participants
            for participant_id in other_participants_ids:
                if usr_ixn.user_online(txn, user_id=participant_id):
                    outgoing_messages.append(Outbound(usr_ixn.get_user_ip_from_id(txn, user_id=participant_id),
                                                      ChatNotification(chat_id,
                                                                       ChatNotification.Notification.DELETED_CHAT,
                                                                       user=deleter_username,
                                                                       time=datetime.datetime.now())))
                else:
                    chat_ixn.create_pending_chat_notification(txn,
                                                              chat_id=chat_id,
                                                              recipient_id=participant_id,
                                                              chat_event=ChatNotification.Notification.DELETED_CHAT,
                                                              username=deleter_username,
                                                              time=datetime.datetime.now())
        else:
            raise ChatException(response_command, "Unknown User")

    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages


def add_user_to_chat(txn, ip, chat_id, username):
    #If ip is from the creator and username exists, add the user to the chat
    #Send an OK response back to the creator
    #Create a chat notification, store it in the DB
    #get list of all chat participants except the creator, send them a new user joined chat message
    response_command = "add_user_to_chat"
    outgoing_messages = list()

    creator = usr_ixn.get_user_id_from_ip(txn, ip=ip)

    try:
        if creator is not None:
            chat_owner = chat_ixn.get_chat_owner(txn, chat_id)

            if chat_owner is None:
                raise ChatException(response_command, "Unable to get chat owner for chat_id %s" % (chat_id,))

            if creator == chat_owner:
                user_id = usr_ixn.get_user_id_from_username(txn, username=username)

                if user_id is not None:
                    other_participants_ids = chat_ixn.get_participants_in_chat(txn, chat_id=chat_id)
                    chat_ixn.add_user_to_chat(txn, chat_id=chat_id, user_id=user_id)
                    outgoing_messages.append(Outbound(ip, Response(response_command, Response.CommandStatus.SUCCESS)))


                    participant_id_to_username = dict()

                    def b(user_id):
                        participant_id_to_username[user_id] = usr_ixn.get_username_from_id(txn, user_id)

                    map(lambda x: b(x), other_participants_ids)
                    other_participants_ids.remove(creator)

                    for other_participant in other_participants_ids:
                        if usr_ixn.user_online(txn, user_id=other_participant):
                            outgoing_messages.append(Outbound(usr_ixn.get_user_ip_from_id(txn, other_participant),
                                                              ChatNotification(chat_id=chat_id,
                                                                               event=ChatNotification.Notification.JOINED_CHAT,
                                                                               user=username,
                                                                               time=datetime.datetime.now())))
                        else:
                            chat_ixn.create_pending_chat_notification(txn,
                                                                      chat_id=chat_id,
                                                                      recipient_id=other_participant,
                                                                      chat_event=ChatNotification.Notification.JOINED_CHAT,
                                                                      username=username,
                                                                      time=datetime.datetime.now())

                    outgoing_messages.append(Outbound(usr_ixn.get_user_ip_from_id(txn, user_id=user_id),
                                                      ChatNotification(chat_id=chat_id,
                                                                       event=ChatNotification.Notification.CREATED_CHAT,
                                                                       user=usr_ixn.get_username_from_id(txn, user_id=chat_owner),
                                                                       time=datetime.datetime.now())))

                    for other_participant in other_participants_ids:
                        outgoing_messages.append(Outbound(usr_ixn.get_user_ip_from_id(txn, user_id=user_id),
                                                          ChatNotification(chat_id=chat_id,
                                                                           event=ChatNotification.Notification.JOINED_CHAT,
                                                                           user=usr_ixn.get_username_from_id(txn, user_id=other_participant),
                                                                           time=datetime.datetime.now())))

                else:
                    raise ChatException(response_command, "Unknown user %s" % (username,))
            else:
                raise ChatException(response_command, "Only chat owner may add users")
        else:
            raise ChatException(response_command, "User not connected")

    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages


def remove_user_from_chat(txn, ip, chat_id, username):
    #If ip is from the creator and username exists, remove the user from the chat
    #Send an OK response back to the creator
    #Create a chat notification, store it in the DB
    #get list of all chat participants except the creator, send them a user left chat message
    response_command = "remove_user_from_chat"
    outgoing_messages = list()

    creator = usr_ixn.get_user_id_from_ip(txn, ip=ip)

    try:
        if creator is not None:
            chat_owner = chat_ixn.get_chat_owner(txn, chat_id)

            if chat_owner is None:
                raise ChatException(response_command, "Unable to get chat owner for chat_id %s" % (chat_id,))

            if creator == chat_owner:
                user_id = usr_ixn.get_user_id_from_username(txn, username=username)

                if user_id is not None:
                    other_participants_ids = chat_ixn.get_participants_in_chat(txn, chat_id=chat_id)

                    if user_id not in other_participants_ids:
                        raise ChatException(response_command, "User being removed not in chat")
                    chat_ixn.delete_user_from_chat(txn, chat_id=chat_id, user_id=user_id)
                    outgoing_messages.append(Outbound(ip, Response(response_command, Response.CommandStatus.SUCCESS)))

                    participant_id_to_username = dict()

                    def b(user_id):
                        participant_id_to_username[user_id] = usr_ixn.get_username_from_id(txn, user_id)

                    map(lambda x: b(x), other_participants_ids)
                    other_participants_ids.remove(creator)

                    for other_participant in other_participants_ids:
                        if usr_ixn.user_online(txn, user_id=other_participant):
                            outgoing_messages.append(Outbound(usr_ixn.get_user_ip_from_id(txn, other_participant),
                                                              ChatNotification(chat_id=chat_id,
                                                                               event=ChatNotification.Notification.LEFT_CHAT,
                                                                               user=username,
                                                                               time=datetime.datetime.now())))
                        else:
                            chat_ixn.create_pending_chat_notification(txn,
                                                                      chat_id=chat_id,
                                                                      recipient_id=other_participant,
                                                                      chat_event=ChatNotification.Notification.LEFT_CHAT,
                                                                      username=username,
                                                                      time=datetime.datetime.now())

                else:
                    raise ChatException(response_command, "Unknown user %s" % (username,))
            else:
                raise ChatException(response_command, "Only chat owner may remove users")
        else:
            raise ChatException(response_command, "User not connected")

    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages


def leave_chat(txn, ip, chat_id):
    #If ip is from a chat participant who is not the creator, remove them from the chat
    #Send an OK response back to the participant who is leaving
    #Create a chat notification, store it in the DB
    #get list of all chat participants except the person leaving, send them a user left chat message
    response_command = "leave_chat"
    outgoing_messages = list()

    try:
        leaver = usr_ixn.get_user_id_from_ip(txn, ip=ip)

        if leaver is not None:
            leaver_username = usr_ixn.get_username_from_id(txn, user_id=leaver)
            other_participants_ids = chat_ixn.get_participants_in_chat(txn, chat_id=chat_id)
            participant_id_to_username = dict()

            def b(user_id):
                participant_id_to_username[user_id] = usr_ixn.get_username_from_id(txn, user_id)

            map(lambda x: b(x), other_participants_ids)

            if leaver not in other_participants_ids:
                raise ChatException(response_command, "Can't leave chat you are not a part of")

            other_participants_ids.remove(leaver)

            chat_ixn.delete_user_from_chat(txn, chat_id=chat_id, user_id=leaver)
            outgoing_messages.append(Outbound(ip, Response(response_command, Response.CommandStatus.SUCCESS)))


            for other_participant in other_participants_ids:
                if usr_ixn.user_online(txn, user_id=other_participant):
                    outgoing_messages.append(Outbound(usr_ixn.get_user_ip_from_id(txn, other_participant),
                                                      ChatNotification(chat_id=chat_id,
                                                                       event=ChatNotification.Notification.LEFT_CHAT,
                                                                       user=leaver_username,
                                                                       time=datetime.datetime.now())))
                else:
                    chat_ixn.create_pending_chat_notification(txn,
                                                              chat_id=chat_id,
                                                              recipient_id=other_participant,
                                                              chat_event=ChatNotification.Notification.LEFT_CHAT,
                                                              username=leaver_username,
                                                              time=datetime.datetime.now())
        else:
            raise ChatException(response_command, "User not connected")

    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages

def send_message(txn, ip, chat_id, msg):
    response_command = "send_message"
    outgoing_messages = list()

    try:
        sender = usr_ixn.get_user_id_from_ip(txn, ip=ip)
        if sender is not None:
            sender_username = usr_ixn.get_username_from_id(txn, user_id=sender)
            participants = chat_ixn.get_participants_in_chat(txn, chat_id= chat_id)

            if participants is not None:

                if sender in participants:
                    participants.remove(sender)

                #From participants, find which are online
                time = datetime.datetime.now()
                for participant in participants:
                        if usr_ixn.user_online(txn, user_id=participant):
                            participant_ip = usr_ixn.get_user_ip_from_id(txn, user_id=participant)
                            outgoing_messages.append(Outbound(participant_ip,
                                                              ChatMessage(chat_id=chat_id,
                                                                          sender=sender_username,
                                                                          message=msg,
                                                                          time=str(time))))
                        else:
                            chat_ixn.create_pending_message(txn,
                                                            chat_id=chat_id,
                                                            recipient_id=participant,
                                                            sender=sender_username,
                                                            message_body=msg,
                                                            time=str(time))

        else:
            raise ChatException(response_command, "User not connected")

        outgoing_messages.append(Outbound(ip,
                                          Response(response_command, Response.CommandStatus.SUCCESS)))
    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages

def send_message_single_user_no_id(txn, ip, recipient_username, chat_msg):
    #Wrapper around send message and create chat
    #All that's needed is the recipient username
    #NOTE - This function should only be used under the assumption there will be only one chat between two users
    #and that we are not bothering with multiuserchat
    response_command = "send_message"
    outgoing_messages = list()

    try:
        sender_id = usr_ixn.get_user_id_from_ip(txn, ip=ip)

        if sender_id is not None:
            recipient_id = usr_ixn.get_user_id_from_username(txn, username=recipient_username)

            if recipient_id is not None:
                chat_id = chat_ixn.find_chat_between_users(txn, sender_id, recipient_id)

                if chat_id is None:
                    #Make a chat between these users
                    #Since we are hiding the create chat from the user, not going to collect the
                    #outgoing_messages from create_chat
                    create_chat(txn, ip, [recipient_username])
                    chat_id = chat_ixn.find_chat_between_users(txn, sender_id, recipient_id)
                    outgoing_messages.append(Outbound(usr_ixn.get_user_ip_from_id(txn, recipient_id),
                                                      ChatNotification(chat_id=chat_id,
                                                                       event=ChatNotification.Notification.CREATED_CHAT,
                                                                       user=usr_ixn.get_username_from_id(txn, user_id=sender_id),
                                                                       time=datetime.datetime.now())))

                if chat_id is not None:
                    outgoing_messages += send_message(txn, ip=ip, chat_id=chat_id, msg=chat_msg)
                else:
                    raise ChatException(response_command,
                                        "No chat exists between users %s and %s and could not create it"
                                        % (usr_ixn.get_username_from_id(txn, sender_id), recipient_id))

            else:
                raise ChatException(response_command, "Recipient with username %s does not exist." % (recipient_username,))
        else:
            raise ChatException(response_command, "User not connected")


    except ChatException as e:
        raise e
    except Exception as e:
        raise ChatException(response_command, e.message)

    return outgoing_messages
